
const request = (url, method = 'GET', data = {}, showToast = true) => {
  return new Promise((resolve, reject) => {
    const token = uni.getStorageSync('token');
    if (token) {
      data.token = token;
    }
    uni.request({
      url: 'https://w2.tnaot.com' + url,
      data,
      params: data,
      method,
      header: {
        'content-type': 'application/json',
      },
      // success: (res) => {
      //   if (res.data.code == 0) {
      //     resolve(res.data)
      //   } else if (res.data.code != 0 && showToast) {
      //     uni.showToast({
      //       title: res.data.message || "请求出错，请稍后重试",
      //       icon: "none",
      //       duration: 2000,
      //     });
      //   }
      // },
      // fail(error) {
      //   if (showToast) {
      //     uni.showToast({
      //       title: "请求出错，请稍后重试",
      //       icon: "none",
      //       duration: 2000,
      //     });
      //   }
      //   reject(error);
      // }
      success: (res) => {
        if (res.data.state == 1) {
          resolve(res.data.result)
        } else if (res.data.state != 0 && showToast) {
          uni.showToast({
            title: res.data.message || "请求出错，请稍后重试",
            icon: "none",
            duration: 2000,
          });
        }
      },
      fail(error) {
        if (showToast) {
          uni.showToast({
            title: "请求出错，请稍后重试",
            icon: "none",
            duration: 2000,
          });
        }
        reject(error);
      }
    });
  })
}

//登录
export function login(data) {
  return request('/applet/login', 'POST', data)
}

//获取列表
export function scenicList(data) {
  return request('/subject/list_contents', 'get', data)
}

